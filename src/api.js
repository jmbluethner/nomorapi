/**
 * @author bluethner <bluethner@heliophobix.com>
 */

const config = require('./config/config');
const dbconnection = require('./lib/js/dbconnector');
const express = require('express');


const app = express();

// Import Routes
// const authRoute = require('./routes/auth');
const ressourceRoute = require('./routes/ressources');

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', '*');
    res.setHeader('Access-Control-Allow-Credentials', 'true');
    next();
});
app.use(express.json());

// API Middlewares
app.use('/api/auth',authRoute);
app.use('/api/ressource',ressourceRoute);

// Admin Dashboard
app.use('/api/admin',express.static('admin'));

app.listen(config.apiPort, () => {

});