const permissions = {
    //  accessOwnerOnly: boolean,       |  Only the owner (creator) of the dataset can access it. Overwrites all privileges.
    //  privileges: [                   |  Define rwd privileges
    //      'table': {                  |  Table name goes here
    //          get: permissionLevel,   |  Required permission level for GET requests (use 'public' for public or 'owner' to only grant access to the dataset owner)
    //          post: permissionLevel,  |  Required permission level for POST requests (use 'public' for public or 'owner' to only grant access to the dataset owner)
    //          put: permissionLevel,   |  Required permission level for PUT requests (use 'public' for public or 'owner' to only grant access to the dataset owner)
    //          delete: permissionLevel |  Required permission level for DELETE requests (use 'public' for public or 'owner' to only grant access to the dataset owner)
    //      }
    //  ]
}

module.exports = permissions;