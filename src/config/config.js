/**
 * @const config
 * @type {{apiPort: number, logLevel: number, logFile: string, outputLogToConsole: boolean}}
 * @description Constant config variable to define some basic settings for the API
 */

const config = {
    // Exposed API port
    apiPort: 3000,
    // Log levels are:
    // 0 - No logging
    // 1 - Only errors
    // 2 - Errors and warnings
    // 3 - All (errors, warnings and notices)
    logLevel: 3,
    // Defines the location where log files will be stored
    logFile: 'logs/',
    // If set to true, everything that gets written to the log will also be shown in the console
    outputLogToConsole: true
}

module.exports = config;