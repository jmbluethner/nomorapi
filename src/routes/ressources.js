const router = require('express').Router();
const verifyToken = require('../lib/js/token-verifier');
const logger = require('../lib/js/logger')

function checkTableVariable(req,res) {
    if(req.query.table) {

    } else {
        logger.log('Client sent an API request but didn\'t supply a table name!',2,req);
        res.status(400).send({error: 'Missing table name!'});
    }
}

function checkPermissions(req,res) {

}

router.get('/private/',verifyToken,(req,res) => {
    checkTableVariable(req,res);
});

router.post('/private/',verifyToken,(req,res) => {
    checkTableVariable(req,res);
});

router.put('/private/',verifyToken,(req,res) => {
    checkTableVariable(req,res);
});

router.delete('/private/',verifyToken,(req,res) => {
    checkTableVariable(req,res);
});

router.get('/public/',(req,res) => {
    checkTableVariable(req,res);
});

router.post('/public/',(req,res) => {
    checkTableVariable(req,res);
});

router.put('/public/',(req,res) => {
    checkTableVariable(req,res);
});

router.delete('/public/',(req,res) => {
    checkTableVariable(req,res);
});

module.exports = router;