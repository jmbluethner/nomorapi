/**
 * @author jmbluethner <bluethner@lucentric.de>n
 * @description Db-Object
 */

const config = require('../../config/config');
const dotenv = require('dotenv');
const mongoose = require('mongoose');

dotenv.config();

mongoose.connect(process.env.db_login, {useNewUrlParser: true, useUnifiedTopology: true}, (err) => {
    if(err) {

    }
});
