const config = require('../../config/config');
const fs = require('fs');

/**
 * @function
 * @returns {string} - Current date and time, formatted for the logger
 */
function logTimestamp() {
    let date = new Date().toLocaleDateString();
    let time = new Date().toLocaleTimeString();
    return '['+date+' | '+time+'] - ';
}

/**
 * @function
 * @returns {string} - Current date, formatted for the log file name
 */
function logFileTimestamp() {
    let date_ob = new Date();
    let date = ("0" + date_ob.getDate()).slice(-2);
    let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
    let year = date_ob.getFullYear();
    return year + "-" + month + "-" + date;
}

/**
 * @function
 * @param {string} logfile - The log file which has to be looked for
 * @param {function} callback - Callback function
 * @returns {boolean} - Returns an error object if there was an error
 */
function checkIfLogFileExists(logfile,callback) {
    try {
        if (!fs.existsSync(logfile)) {
            // File doesn't exist, so it gets created.
            fs.writeFile(logfile, logTimestamp()+'CREATED LOG FILE\r\n', function (err) {
                if (err) {
                    console.log('[!] Error while trying to create log file: '+err);
                    throw err;
                } else {
                    console.log('[!] Created log file');
                }
            });
        }
        callback();
    } catch(err) {
        return JSON.stringify({error: err});
    }
}

/**
 * @function
 * @param {string} message - The message you want to show in the log
 * @param {number} logLevel - The log level for the message
 * @param {Object} req - The API request which triggered the log message if there is one
 */
function log(message, logLevel,req = null) {
    if(logLevel <= config.logLevel) {
        let logfile = config.logFile+logFileTimestamp()+'.log';
        checkIfLogFileExists(logfile,function () {
            if(req != null) {
                fs.appendFile(logfile,logPrefix+'[ '+req.connection.remoteAddress+'. '+req.method+' -> '+req.originalUrl+' ] '+txt+'\r\n',function (err) {
                    if(config.outputLogToConsole) {
                        console.log(logPrefix+'[ '+req.connection.remoteAddress+'. '+req.method+' -> '+req.originalUrl+' ] '+txt);
                    }
                    if(err) return console.log(err);
                });
            } else {
                fs.appendFile(logfile,logPrefix+''+txt+'\r\n',function (err) {
                    if(config.outputLogToConsole) {
                        console.log(logPrefix+''+txt);
                    }
                    if(err) return console.log(err);
                });
            }
        });
    } else {
        // Message hidden because the defined log level was too low
        return false;
    }
}

module.exports = {
    log: log
};