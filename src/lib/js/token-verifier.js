const jwt = require('jsonwebtoken');

module.exports = function(req,res,next) {
    // Get token
    const token = req.header('auth-token');
    // return if no token was found
    if(!token) return res.status(401).send({error:'Access Denied!'});

    try {
        // try to verify token
        req.user = jwt.verify(token,process.env.token_secret);
        next();
    } catch(err) {
        // Verification went wrong, return
        res.status(400).send({error: 'Invalid API Token'});
    }
}